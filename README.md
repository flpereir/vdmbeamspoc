# VdMBeamsPOC

This is a mere proof of concept to show how much faster the beam current calculation can be when
leveraging the power of numpy and pandas. 

Disclaimers:
- FBCT to DCCT calibration is not performed. Diminishing returns were observed in benchmarking.
- Assumes pp beamsin run3.

## Run

```console
python main.py
```