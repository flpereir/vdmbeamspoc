"""Proof of concept

This is a mere proof of concept to show how much faster the beam current calculation can be when
leveraging the power of numpy and pandas. 

Disclaimers:
- FBCT to DCCT calibration is not performed. Diminishing returns were observed in benchmarking.
- Assumes pp beamsin run3.
"""

from typing import List

import json
import tables
import numpy as np
import pandas as pd


def read_hd5(path: str, node: str, required_columns: List[str]) -> pd.DataFrame:
    with tables.open_file(path, "r") as t:
        node: tables.Table = t.get_node(f"/{node}")

        data = {
            col: (
                node.col(col) if len(node.coldescrs[col].shape) == 0 
                else list(node.col(col))
            ) for col in required_columns
        }

        return pd.DataFrame(data)

# Read Beam Data
beam = read_hd5(
    "/eos/cms/store/group/dpg_bril/comm_bril/vdmdata/2023/original/8999/8999_230628210012_230628214708.hd5",
    "scan5_beam", ["timestampsec", "intensity1", "intensity2", "bxintensity1", "bxintensity2"]
)

# Read Scan Information
with open("/afs/cern.ch/user/f/flpereir/VdMFramework/output_timed/analysed_data/8999_28Jun23_230143_28Jun23_232943/cond/Scan_8999.json") as fp:
    scanInfo = json.load(fp)

# Read Scan Information into DataFrame
data_list = scanInfo["Scan_1"] + scanInfo["Scan_2"]
data = np.stack(data_list)
columns = list(scanInfo["Scans"][0].keys())[1:]
df = pd.DataFrame(data, columns=columns).drop(columns=["XDisplacements", "YDisplacements", "RelativeDisplacements", "LSStartTimes", "LSStopTimes"]).rename(columns={"ScanType":"ScanPoint"})

df["ScanNumber"] = df["ScanNumber"].astype(int)
df["ScanPoint"] = df["ScanPoint"].astype(int)

# Compute timestamp masks
df["time_masks"] = df["StartTimes"].apply(lambda x: f"(timestampsec > {x} & ") + df["StopTimes"].apply(lambda x: f"timestampsec <= {x})")

# Compute DCCT
df["dcctB1"] = df["time_masks"].apply(lambda x: beam.query(x)["intensity1"].mean())
df["dcctB2"] = df["time_masks"].apply(lambda x: beam.query(x)["intensity2"].mean())


# Compute FBCT (assuming pp beams)
pp_threshold = 0.1e10
fbct1 = []
fbctsum1 = []
fbctsumavgr1 = []
fbct2 = []
fbctsum2 = []
fbctsumavgr2 = []
for i in range(df.shape[0]):
    # Filter beam data by time mask
    beam_q = beam.query(df["time_masks"].iloc[i])

    # Compute filled bunches and colliding bunches
    ifilled1 = np.stack(beam_q["bxintensity1"].apply(lambda x: np.where(x > pp_threshold)[0]))
    ifilled2 = np.stack(beam_q["bxintensity2"].apply(lambda x: np.where(x > pp_threshold)[0]))
    icolliding = np.stack([np.intersect1d(ifilled1[i], ifilled2[i]) for i in range(ifilled1.shape[0])])
    
    # Structure frist beam bx data
    bxintensity1 = np.stack(beam_q["bxintensity1"])
    bxdata1 = np.stack([bxintensity1[i][ifilled1[0]] for i in range(bxintensity1.shape[0])])
    bxdata1 = pd.DataFrame(bxdata1, columns=ifilled1[0])

    # Structure second beam bx data
    bxintensity2 = np.stack(beam_q["bxintensity2"])
    bxdata2 = np.stack([bxintensity2[i][ifilled2[0]] for i in range(bxintensity2.shape[0])])
    bxdata2 = pd.DataFrame(bxdata2, columns=ifilled2[0])

    # Compute leading bunches correction (assuming run3 fill)
    amountb1 = np.ones(3564)
    bxmean1 = bxintensity1.mean(axis=0)
    bxmean1_prev = np.roll(bxmean1, 1)
    b1_leading_mask = bxmean1*0.1 > bxmean1_prev
    amountb1[b1_leading_mask] = 0.993

    # Compute leading bunches correction (assuming run3 fill)
    amountb2 = np.ones(3564)
    bxmean2 = bxintensity2.mean(axis=0)
    bxmean2_prev = np.roll(bxmean2, 1)
    b2_leading_mask = bxmean2*0.1 > bxmean2_prev
    amountb2[b2_leading_mask] = 0.993


    # Compute beam 1 FBCT
    fbct1.append(bxdata1.mean() * amountb1[ifilled1[0]])
    fbctsum1.append(fbct1[-1][icolliding[0]].sum())
    fbctsumavgr1.append(fbct1[-1].sum())

    # Compute beam 2 FBCT
    fbct2.append(bxdata2.mean() * amountb2[ifilled2[0]])
    fbctsum2.append(fbct2[-1][icolliding[0]].sum())
    fbctsumavgr2.append(fbct2[-1].sum())

# Convert to DataFrame
fbct1 = pd.DataFrame(np.stack(fbct1), columns=fbct1[0].index + 1)
fbct2 = pd.DataFrame(np.stack(fbct2), columns=fbct2[0].index + 1)
fbctsum1 = np.array(fbctsum1)
fbctsumavgr1 = np.array(fbctsumavgr1)
fbctsum2 = np.array(fbctsum2)
fbctsumavgr2 = np.array(fbctsumavgr2)

# Save to JSON
file = {}
for i in df["ScanNumber"].unique():
    file[f"Scan_{i}"] = []
    rel_df = df[df["ScanNumber"] == i]
    for j in range(rel_df.shape[0]):
        d = rel_df.iloc[j][["ScanNumber", "ScanName", "ScanPoint", "dcctB1", "dcctB2"]].to_dict()
        d["ScanNumber"] = int(d["ScanNumber"])
        d["ScanPoint"] = int(d["ScanPoint"])
        d["sumavrgfbct1"] = fbctsumavgr1[j*i]
        d["sumavrgfbct2"] = fbctsumavgr2[j*i]
        d["fbctB1"] = fbct1.iloc[j*i].to_dict()
        d["fbctB1"]["sum"] = fbctsum1[j*i]
        d["fbctB2"] = fbct2.iloc[j*i].to_dict()
        d["fbctB2"]["sum"] = fbctsum2[j*i]

        file[f"Scan_{i}"].append(d)

with open("Scan_8999.json", "w") as fp:
    json.dump(file, fp, indent=4)
